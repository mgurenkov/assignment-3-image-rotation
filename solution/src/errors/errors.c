#include "errors.h"
char const* const error_messages[] = {
        [READ_INVALID_SIGNATURE] = "Invalid file format",
        [READ_INVALID_BITS] = "Error reading pixel data",
        [READ_INVALID_HEADER] = "Error reading file header",
        [WRITE_ERROR] = "Error writing to file",
        [OPEN_ERROR] = "Error opening file",
        [ANGLE_INVALID] = "Invalid angle",
        [MEMORY_OVERFLOW_ERROR] = "Memory overflow",
        [TOO_MANY_ARGS] = "Too many arguments",
        [TOO_FEW_ARGS] = "Too few arguments"
};

void print_status(enum error const e) {
    if (e != OK) {
        fprintf(stderr, "ERROR: %s", error_messages[e]);
    }
}
