#include "image.h"
void image_init(uint32_t width, uint32_t height, struct pixel* data, struct image* img) {
    img->width = width;
    img->height = height;
    img->data = data;
}
enum error image_malloc(struct image* img, size_t data_size) {
    img->data = malloc(sizeof (struct pixel)* data_size);
    if (img->data == NULL) {
        return MEMORY_OVERFLOW_ERROR;
    }
    return OK;
}

void image_destroy(struct image* img) {
    free(img->data);
}
