#include "bmp.h"
#include "rotate.h"
#include "utils.h"

#define ANGLE 3
#define SOURCE_FILE 1
#define TARGET_FILE 2

int main( int argc, char** argv ) {
    enum error current_status;
    current_status = check_args_quantity(argc);
    if (current_status != OK) {
        print_status(current_status);
        return current_status;
    }
    int32_t angle;
    current_status = get_angle(argv[ANGLE], &angle);
    if (current_status != OK) {
        print_status(current_status);
        return current_status;
    }
    FILE *f;
    current_status = get_file(argv[SOURCE_FILE], &f, "rb");
    if (current_status != OK) {
        print_status(current_status);
        return current_status;
    }
    struct image img;
    current_status = from_bmp(f, &img);
    if (current_status != OK) {
        print_status(current_status);
        return current_status;
    }
    current_status = rotate(&img, angle);
    if (current_status != OK) {
        print_status(current_status);
        return current_status;
    }
    current_status = get_file(argv[TARGET_FILE], &f, "wb");
    if (current_status != OK) {
        print_status(current_status);
        image_destroy(&img);
        return current_status;
    }
    current_status = to_bmp(f, &img);
    image_destroy(&img);
    print_status(current_status);
    return current_status;
}

