#include "errors.h"
#include <stdlib.h>
#define MAX_ARGS 4

enum error get_file(char* str_arg, FILE** f, char* mode) {
    *f = fopen(str_arg, mode);
    if (*f) {
        return OK;
    }
    fclose(*f);
    return OPEN_ERROR;
}

enum error get_angle(char const* const str_arg, int32_t* const int_angle) {
    int32_t valid_angles[] = {0, 90, -90, 180, -180, 270, -270};
    char* endptr = 0;
    *int_angle = (int32_t) strtol(str_arg, &endptr, 10);
    for (size_t i = 0; i < sizeof(valid_angles)/sizeof(int32_t); i++) {
        if (*int_angle == valid_angles[i]) {
            return OK;
        }
    }
    return ANGLE_INVALID;
}

enum error check_args_quantity(int const argc) {
    if (argc > MAX_ARGS) {
        return TOO_MANY_ARGS;
    } else if (argc < MAX_ARGS) {
        return TOO_FEW_ARGS;
    } else {
        return OK;
    }
}
