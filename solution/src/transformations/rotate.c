#include "image.h"
#define ANGLE_MULTIPLICITY 90
enum error rotate_m90(struct image* img, int32_t times) {
    for (int32_t i = 0; i < times; i ++) {
        struct pixel* new_pixels = malloc(sizeof (struct pixel)*img->width*img->height);
        if (new_pixels != NULL) {
            size_t counter = 0;
            for (size_t w = 0; w < img->width; w ++) {
                for (int64_t h = (int64_t) img->height - 1; h > -1; h --) {
                    *(new_pixels + counter) = *(img->data + h*img->width + w);
                    counter++;
                }
            }
            free(img->data);
            image_init(img->height, img->width, new_pixels, img);
        } else {
            image_destroy(img);
            return MEMORY_OVERFLOW_ERROR;
        }
    }
    return OK;
}

enum error rotate_90(struct image* img, int32_t times) {
    for (int32_t i = 0; i < times; i ++) {
        struct pixel* new_pixels = malloc(sizeof (struct pixel)*img->width*img->height);
        if (new_pixels != NULL) {
            size_t counter = 0;
            for (int64_t w = (int64_t) img->width - 1; w > -1; w --) {
                for (size_t h = 0; h < img->height; h ++) {
                    *(new_pixels + counter) = *(img->data + h*img->width + w);
                    counter++;
                }
            }
            free(img->data);
            image_init(img->height, img->width, new_pixels, img);
        } else {
            image_destroy(img);
            return MEMORY_OVERFLOW_ERROR;
        }
    }
    return OK;
}

enum error rotate(struct image* img, int32_t const angle) {
    int32_t times = angle / ANGLE_MULTIPLICITY;
    if (angle >= 0) {
        return rotate_90(img, times);
    } else {
        return rotate_m90(img, times*(-1));
    }
}
