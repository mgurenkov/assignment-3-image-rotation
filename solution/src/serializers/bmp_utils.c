#include "bmp_utils.h"

#define MAX_PADDING 4

const uint8_t padding_bytes[MAX_PADDING] = {0};
uint8_t get_padding(uint32_t width) {
    return 4 - width*sizeof(struct pixel) % 4;
}

enum error check_file_signature(FILE* in) {
    int8_t bfType[2];
    fseek(in, 0, SEEK_SET);
    fread(bfType, sizeof(char), 2, in);
    if (bfType[0] == 'B' && bfType[1] == 'M') {
        return OK;
    } else {
        return READ_INVALID_SIGNATURE;
    }
}

enum error read_header(FILE* in, struct bmp_header* header) {
    fseek(in, 0, SEEK_SET);
    if (fread(header, sizeof(*header), 1, in)) {
        return OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

enum error read_pixels(FILE* in, struct bmp_header* header, struct pixel* buffer) {
    fseek(in, header->bOffBits, SEEK_SET);
    uint8_t padding = get_padding(header->biWidth);
    size_t pixels_was_read = 0;
    for (size_t i = 0; i < header->biHeight; i ++) {
        pixels_was_read += fread((buffer + pixels_was_read), sizeof(struct pixel), header->biWidth, in);
        fseek(in, padding, SEEK_CUR);
    }
    if (pixels_was_read == header->biWidth*header->biHeight) {
        return OK;
    } else {
        return READ_INVALID_BITS;
    }
}

enum error write_header(FILE* out, struct bmp_header const* const header) {
    fseek(out, 0, SEEK_SET);
    if (fwrite(header, sizeof(struct bmp_header), 1, out)) {
        return OK;
    } else {
        return WRITE_ERROR;
    }
}

enum error write_pixels(FILE* out, struct bmp_header* header, struct pixel const* const data) {
    fseek(out, header->bOffBits, SEEK_SET);
    size_t pixels_was_wrote = 0;
    for (size_t i = 0; i < header->biHeight; i++) {
        pixels_was_wrote += fwrite((data + pixels_was_wrote), sizeof(struct pixel), header->biWidth, out);
        fwrite(&padding_bytes, 1, get_padding(header->biWidth), out);
    }
    if (pixels_was_wrote == header->biWidth * header->biHeight) {
        return OK;
    } else {
        return WRITE_ERROR;
    }
}

void swap_header_fields(struct bmp_header* header) {
    uint32_t buf = header->biHeight;
    header->biHeight = header->biWidth;
    header->biWidth = buf;
    buf = header->biXPelsPerMeter;
    header->biXPelsPerMeter = header->biYPelsPerMeter;
    header->biYPelsPerMeter = buf;
}
