#include "bmp_utils.h"
#include "image.h"

struct bmp_header header;

enum error current_status;

enum error from_bmp( FILE* in, struct image* img ) {
    current_status = check_file_signature(in);
    if (current_status == OK) {
        current_status = read_header(in, &header);
        if (current_status == OK) {
            current_status = image_malloc(img, header.biWidth*header.biHeight);
            if (current_status == OK) {
                current_status = read_pixels(in, &header, img->data);
                img->width = header.biWidth;
                img->height = header.biHeight;
            } else {
                image_destroy(img);
            }
        }
    }
    fclose(in);
    return current_status;
}

enum error to_bmp( FILE* out, struct image* img ) {
    if (img->width == header.biHeight) {
        swap_header_fields(&header);
    }
    current_status = write_header(out, &header);
    if (current_status == OK) {
        current_status = write_pixels(out, &header, img->data);
    }
    fclose(out);
    return current_status;
}
