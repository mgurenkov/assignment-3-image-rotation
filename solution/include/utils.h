#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H
#include "errors.h"
enum error get_file(char* str_arg, FILE** f, char* mode);
enum error get_angle(char const* const str_arg, int32_t* const int_angle);
enum error check_args_quantity(int const argc);
#endif //IMAGE_TRANSFORMER_UTILS_H
