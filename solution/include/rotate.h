#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"
enum error rotate(struct image* img, int32_t const angle);
#endif //IMAGE_TRANSFORMER_ROTATE_H
