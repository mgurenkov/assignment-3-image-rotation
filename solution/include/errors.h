#ifndef IMAGE_TRANSFORMER_ERRORS_H
#define IMAGE_TRANSFORMER_ERRORS_H
#include <inttypes.h>
#include <stdio.h>
enum error  {
    OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    WRITE_ERROR,
    OPEN_ERROR,
    ANGLE_INVALID,
    MEMORY_OVERFLOW_ERROR,
    TOO_MANY_ARGS,
    TOO_FEW_ARGS
};
void print_status(enum error const e);
#endif //IMAGE_TRANSFORMER_ERRORS_H
