#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H
#include "image.h"
enum error from_bmp( FILE* in, struct image* img );
enum error to_bmp( FILE* out, struct image* img );
#endif //IMAGE_TRANSFORMER_BMP_H
