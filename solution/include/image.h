#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H
#include "errors.h"
#include <malloc.h>
struct pixel {
    uint8_t b, g, r;
};
struct image {
    uint32_t width, height;
    struct pixel* data;
};
void image_init(uint32_t width, uint32_t height, struct pixel* data, struct image* img);
enum error image_malloc(struct image* img, size_t data_size);
void image_destroy(struct image* img);
#endif //IMAGE_TRANSFORMER_IMAGE_H
